package main

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"
	//"strings"
)

var ignored = map[string]bool{
	".DS_Store":  true,
	".idea":      true,
	".git":       true,
	"dockerfile": true,
	"hw1.md":     true,
}

func dirTreeDepth(out io.Writer, path string, printFiles bool, prefix string, prefix2 string) (err error) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		return err
	}

	var filteredFiles []os.FileInfo
	for _, f := range files {
		if _, exists := ignored[f.Name()]; !exists {
			if printFiles || f.IsDir() {
				filteredFiles = append(filteredFiles, f)
			}
		}
	}

	//fmt.Printf("%v\n", files)

	for i, f := range filteredFiles {

		if _, ok := ignored[f.Name()]; !ok {
			size := ""
			if !f.IsDir() {
				if f.Size() == 0 {
					size = " (empty)"
				} else {
					size = fmt.Sprintf(" (%vb)", f.Size())
				}
			}

			if printFiles || f.IsDir() {
				sep := "├"
				if len(filteredFiles)-1 == i {
					sep = "└"
				}
				_, _ = fmt.Fprintln(out, prefix+sep+"───"+f.Name()+size)

			}

			if f.IsDir() {
				if len(filteredFiles)-1 == i {
					prefix2 = ""
				}
				err = dirTreeDepth(out, path+string(os.PathSeparator)+f.Name(), printFiles, prefix+prefix2+"\t", "│")
				if err != nil {
					return err
				}
			}
		}
		//log.Println(f.Name())
	}

	return nil
}

func dirTree(out io.Writer, path string, printFiles bool) (err error) {
	return dirTreeDepth(out, path, printFiles, "", "│")
}

func main() {
	out := os.Stdout
	if !(len(os.Args) == 2 || len(os.Args) == 3) {
		panic("usage go run main.go . [-f]")
	}
	path := os.Args[1]
	printFiles := len(os.Args) == 3 && os.Args[2] == "-f"
	err := dirTree(out, path, printFiles)
	if err != nil {
		panic(err.Error())
	}
}
